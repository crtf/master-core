﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RocketCore
{
    static class Flags
    {
        internal static bool market_closed = false; //флаг отклонения вызовов SET-функций юзера (+ предохранитель для RestoreSnapshot)
        internal static bool backup_restore_in_proc = false; //флаг отклонения вызовов всех функций
        
        internal static StatusCodes CloseMarket()
        {
            if (!market_closed)
            {
                market_closed = true;
                Pusher.NewMarketStatus((int)MarketStatus.Closed, DateTime.Now); //сообщение о новом статусе рынка
                return StatusCodes.Success;
            }
            else return StatusCodes.ErrorMarketAlreadyClosed;
        }

        internal static StatusCodes OpenMarket()
        {
            if (market_closed)
            {
                market_closed = false;
                Pusher.NewMarketStatus((int)MarketStatus.Opened, DateTime.Now); //сообщение о новом статусе рынка
                return StatusCodes.Success;
            }
            else return StatusCodes.ErrorMarketAlreadyOpened;
        }
        
    }
}
