﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RocketCore.Gateways.TCP.Messages
{
    class TickerMsg : IJsonSerializable
    {
        private decimal bid;
        private decimal ask;
        private DateTime dt_made;

        internal TickerMsg(decimal bid, decimal ask, DateTime dt_made) //конструктор сообщения
        {
            this.bid = bid;
            this.ask = ask;
            this.dt_made = dt_made;
        }

        public string Serialize()
        {
            return JsonManager.FormTechJson((int)MessageTypes.NewTicker, bid, ask, dt_made);
        }
    }
}
