﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using QuickFix;

namespace RocketCore
{
    class FixManager
    {
        //FIX-объекты
        private SessionSettings settings;
        private FixAcceptor fixApp;
        private IMessageStoreFactory storeFactory;
        private ThreadedSocketAcceptor acceptor;

        internal FixManager()
        {
            //запуск FIX-акцептора
            try
            {
                settings = new SessionSettings();

                Dictionary d = new Dictionary();
                DateTime dt = DateTime.Now;
                d.SetString("FileLogPath", @"fix\log" + "_" + dt.Year + "_" + dt.Month + "_" + dt.Day + "_" + dt.Hour + "_" + dt.Minute + "_" + dt.Second); 
                d.SetString("FileStorePath", @"fix\store" + "_" + dt.Year + "_" + dt.Month + "_" + dt.Day + "_" + dt.Hour + "_"+ dt.Minute + "_" + dt.Second);                
                d.SetString("ConnectionType", "acceptor");
                d.SetString("StartTime", "00:00:00");
                d.SetString("EndTime", "00:00:00");
                d.SetLong("SocketAcceptPort", 9201);
                d.SetBool("ResetOnLogon", true);
                d.SetBool("ResetOnLogout", true);
                d.SetBool("ResetOnDisconnect", true);
                d.SetBool("UseDataDictionary", true);
                d.SetString("DataDictionary", @"fix\FIX44.xml");
                
                settings.Set(d);

                //добавление нового клиента
                SessionID mySessionID = new SessionID("FIX.4.4", "ROCKETBTC", "RCLT_1_1");
                settings.Set(mySessionID, new Dictionary());
                SessionID mySessionID2 = new SessionID("FIX.4.4", "ROCKETBTC", "RCLT_1_2");
                settings.Set(mySessionID2, new Dictionary());
                                
                fixApp = new FixAcceptor();
                storeFactory = new FileStoreFactory(settings);
                acceptor = new ThreadedSocketAcceptor(fixApp, storeFactory, settings);
                
                acceptor.Start();
                
            }
            catch (Exception e)
            {
                Console.WriteLine("==FIX START ERROR==");
                Console.WriteLine(e.ToString());
            }
        }

        internal StatusCodes RestartApp(string[] sender_comp_ids)
        {
            try
            {
                acceptor.Stop(true);

                settings = new SessionSettings();

                Dictionary d = new Dictionary();
                DateTime dt = DateTime.Now;
                d.SetString("FileLogPath", @"fix\log" + "_" + dt.Year + "_" + dt.Month + "_" + dt.Day + "_" + dt.Hour + "_" + dt.Minute + "_" + dt.Second);
                d.SetString("FileStorePath", @"fix\store" + "_" + dt.Year + "_" + dt.Month + "_" + dt.Day + "_" + dt.Hour + "_" + dt.Minute + "_" + dt.Second);
                d.SetString("ConnectionType", "acceptor");
                d.SetString("StartTime", "00:00:00");
                d.SetString("EndTime", "00:00:00");
                d.SetLong("SocketAcceptPort", 9201);
                d.SetBool("ResetOnLogon", true);
                d.SetBool("ResetOnLogout", true);
                d.SetBool("ResetOnDisconnect", true);
                d.SetBool("UseDataDictionary", true);
                d.SetString("DataDictionary", @"fix\FIX44.xml");

                settings.Set(d);

                //обновление FIX-аккаунтов
                int length = sender_comp_ids.Length;
                if (length > 0)
                {
                    for (int i = 0; i < length; i++)
                    {
                        SessionID mySessionID = new SessionID("FIX.4.4", "ROCKETBTC", sender_comp_ids[i]);
                        settings.Set(mySessionID, new Dictionary());
                    }
                }
                else
                {
                    SessionID mySessionID = new SessionID("FIX.4.4", "ROCKETBTC", "RCLT_1_1");
                    settings.Set(mySessionID, new Dictionary());
                    SessionID mySessionID2 = new SessionID("FIX.4.4", "ROCKETBTC", "RCLT_1_2");
                    settings.Set(mySessionID2, new Dictionary());
                }

                fixApp = new FixAcceptor();
                storeFactory = new FileStoreFactory(settings);
                acceptor = new ThreadedSocketAcceptor(fixApp, storeFactory, settings);

                acceptor.Start();
                Sys.core.MarkFixAccountsActive(); //отметка, что текущие FIX-аккаунты ядра вступили в силу
                Sys.fixcast.ResetMsgSeqNums(); //сброс порядковых номеров FIX multicast сообщений

                Pusher.NewFixRestart((int)StatusCodes.Success, DateTime.Now); //сообщение о перезапуске FIX-приложения
                return StatusCodes.Success;
            }
            catch (Exception e)
            {
                Console.WriteLine("==FIX RESTART ERROR==");
                Console.WriteLine(e.ToString());

                Pusher.NewFixRestart((int)StatusCodes.ErrorFixRestartFailed, DateTime.Now); //сообщение о перезапуске FIX-приложения
                return StatusCodes.ErrorFixRestartFailed;
            }
        }

        internal void RemoveFixAccountQueue(string sender_comp_id)
        {
            ConcurrentQueue<Message> queue;
            Queues.fix_dict.TryRemove(sender_comp_id, out queue);
        }

    }
}
